import { Validator } from "../src/validator";


describe("Validator NIF", () => {
    let val
    beforeEach(() => {
        val = new Validator;
    })

    it('NIF', () => {
        const key = "06288001P";

        expect(val.NIF(key)).toEqual("NIF correcto");
    })
})
