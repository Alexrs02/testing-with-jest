import { PairOdd } from "../src/pairOdd";


describe("Pair or Odd", () => {
    let pairOdd
    beforeEach(() => {
        pairOdd = new PairOdd;
    })

    it('esPar', () =>{
        const num = 2;
        expect(pairOdd.comparate(num)).toEqual('Es par');
    })
})