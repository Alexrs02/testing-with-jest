import { Comprobator } from "../src/comprobator";


describe ("Comprobator",()=>{
    let comprob
    beforeEach(() =>{
        comprob = new Comprobator;
    })

    it('palindromo',() =>{
        const value = "Yo dono rosas, oro no doy"

        expect(comprob.isPalindromo(value)).toEqual("Es palíndromo")
    })
})