export class Comprobator{
    isPalindromo(value){
        const reset = /[\W_]/g;
        const lowReset = value.toLowerCase().replace(reset,'');
        const reversevalue = lowReset.split('').reverse().join('');
        if(reversevalue === lowReset){
            return "Es palíndromo"
        }else{
            return "No es palíndromo"
        }
    }
}