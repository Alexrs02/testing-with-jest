export class Validator {
    NIF(value) {
        if (value.length === 9) {
            const letter = value.charAt(value.length - 1) ;
            const numbers = value.slice(1, value.length - 1);
            const posibleLetters = "TRWAGMYFPDXBNJZSQVHLCKE";

            const calNum = (numbers % 23);
            const calLetter = posibleLetters.charAt(calNum);
            if (letter === calLetter) {
                return "NIF correcto";
            } else {
                return "NIF no coincide con la letra";
            }
        } else {
            return "No son 9 caracteres";
        }
    }
}